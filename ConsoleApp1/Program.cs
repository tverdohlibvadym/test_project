﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Введіть номер будинку - ");
            string fileName = Console.ReadLine();
           
            
            List<string> numberList = new List<string>();
            List<string> lenghtList = new List<string>();

            var fs = File.ReadAllLines(@"House-" + fileName + ".csv").Select(a => a.Split(','));
            int counter = 0;
            foreach (var line in fs)
            {
                counter++;
                if (counter > 1)
                {
                    numberList.Add(line[1]);
                    lenghtList.Add(line[2]);
                }
            }
            
            List<double> t = new List<double>();
            string[] numberArray = numberList.ToArray();
            string[] lenghtArray = lenghtList.ToArray();
            for (int i = 0; i < numberArray.Length; i++)
            {
                for (int j = 0; j < Convert.ToInt32(lenghtArray[i]); j++)
                {
                    numberArray = numberArray.Select(s => s.Replace(".", ",")).ToArray();
                    t.Add(Convert.ToDouble(numberArray[i]));
                }
            }
            var selectedList = t.OrderByDescending(u=>u);

            int count = 0;
            Dictionary<int, double> dict = selectedList.ToDictionary(i => count++);

            int numberOfLogs = 0;
            double totalRemainder = 0;
            double totalSum = 0;


            using StreamWriter sw = File.CreateText(@"House-" + fileName + ".txt");
            foreach(var (key, value) in dict)
            {
                numberOfLogs++;
                var myValue = value;
                double difference = 6;
                sw.Write(value + " ");
                double sum = 0;
                for (var i = 0; i < count; i++)
                {
                    dict.Remove(key);
                    difference -= myValue;
                    myValue = dict.FirstOrDefault(x => x.Value <= difference).Value;
                    sum += myValue;
                    if(myValue == 0) break;
                    
                    var myKey = dict.First(x => x.Value <= difference).Key;
                    
                    sw.Write(myValue + " ");
                    
                    dict.Remove(myKey);
                }
                
                sum += value;
                var remainder = 6 - sum;
                if (remainder < 0)
                    remainder = 0;
                totalSum += sum;
                totalRemainder += remainder;
                sw.Write("Довжина = " + Math.Round(sum, 2) + " ");
                sw.Write("Залишок = " + Math.Round(remainder, 2) + " ");
                
                sw.WriteLine();
            }
            sw.WriteLine("Потрібно матеріалу = " + Math.Round(totalSum, 2));
            sw.WriteLine("Загальний залишок = " + Math.Round(totalRemainder, 2));
            sw.WriteLine("Кількість потрібних колод = " + numberOfLogs);
            Console.Write("Результати в файлі House-" + fileName);
        }
    }
}

